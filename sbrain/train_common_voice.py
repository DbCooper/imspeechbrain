import glob
import json
import logging
import os
import re
from speechbrain.dataio.dataio import read_audio

logger = logging.getLogger(__name__)

SAMPLERATE = 16000

regex_char = ['"', '%', ".",  "'",  '(',  ')', '̣',  '*', '|','+','–','?', '̉', '̀', '́' ,  ',',  '-',  '/',':', '”','`', '“', '̃',';','\\\\','@']
chars_to_ignore_regex = f"[{''.join(regex_char)}]"

def prepare_common_voice(
        data_folder, save_json_train, save_json_valid, save_json_test
):
    """
    Prepares the json files for the Mini Librispeech dataset.
    Downloads the dataset if its not found in the `data_folder`.
    Arguments
    ---------
    data_folder : str
        Path to the folder where the Mini Librispeech dataset is stored.
    save_json_train : str
        Path where the train data specification file will be saved.
    save_json_valid : str
        Path where the validation data specification file will be saved.
    save_json_test : str
        Path where the test data specification file will be saved.
    Example
    -------
    >>> data_folder = '/path/to/mini_librispeech'
    >>> prepare_common_voice(data_folder, 'train.json', 'valid.json', 'test.json')
    """

    # Check if this phase is already done (if so, skip it)
    wav_list = glob.glob(f"{data_folder}/wavs/*")
    split_train = int(len(wav_list) * 0.8)
    split_eval = int(len(wav_list) * 0.9)

    trans_dict = get_transcription(data_folder)

    # Create the json files
    create_json(wav_list[:split_train], trans_dict, save_json_train)
    create_json(wav_list[split_train:split_eval], trans_dict, save_json_valid)
    create_json(wav_list[split_eval:], trans_dict, save_json_test)


def get_transcription(data_folder):
    """
    Returns a dictionary with the transcription of each sentence in the dataset.
    Arguments
    ---------
    data_folder : str
        The path to metadata.csv
    """
    # Processing all the transcription files in the list

    trans_dict = {}
    with open(os.path.join(data_folder, "metadata.csv"), "r") as f:
        for line in f.readlines():
            uttid = line.split("|")[0]
            uttid = ".".join(uttid.split(".")[:-1])
            text = line.rstrip().split("|")[1:]
            text = " ".join(text)
            text = unicodedata.normalize("NFC", text.lower())
            text = re.sub(chars_to_ignore_regex, "", l)
            trans_dict[uttid] = text

    logger.info("Transcription files read!")
    return trans_dict


def create_json(wav_list, trans_dict, json_file):
    """
    Creates the json file given a list of wav files and their transcriptions.
    Arguments
    ---------
    wav_list : list of str
        The list of wav files.
    trans_dict : dict
        Dictionary of sentence ids and word transcriptions.
    json_file : str
        The path of the output json file
    """
    # Processing all the wav files in the list
    json_dict = {}
    for wav_file in wav_list:
        # Reading the signal (to retrieve duration in seconds)
        signal = read_audio(wav_file)
        duration = signal.shape[0] / SAMPLERATE

        # Manipulate path to get relative path and uttid
        path_parts = wav_file.split(os.path.sep)
        uttid, _ = os.path.splitext(path_parts[-1])
        relative_path = os.path.join("{data_root}", *path_parts[-5:])

        if duration < 1.0 or duration > 16.0: continue
        # Create entry for this utterance
        if trans_dict.get(uttid):
            json_dict[uttid] = {
                "wav": relative_path,
                "duration": duration,
                "wrd": trans_dict[uttid],
            }

    # Writing the dictionary to the json file
    with open(json_file, mode="w",encoding='utf-8') as json_f:
        json.dump(json_dict, json_f, indent=2, ensure_ascii=False)

    logger.info(f"{json_file} successfully created!")


def skip(*filenames):
    """
    Detects if the data preparation has been already done.
    If the preparation has been done, we can skip it.
    Returns
    -------
    bool
        if True, the preparation phase can be skipped.
        if False, it must be done.
    """
    for filename in filenames:
        if not os.path.isfile(filename):
            return False
    return True


def check_folders(*folders):
    """Returns False if any passed folder does not exist."""
    for folder in folders:
        if not os.path.exists(folder):
            return False
    return True
