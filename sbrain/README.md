# imspeechbrain

## step1
- run `python tokenizer.train hparams/<config>.yaml`

## step2
- run `python lm.train hparams/<config>.yaml`

## step3
- run `python sbrain.<model_name> hparams/<config>.yaml`

